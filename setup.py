from distutils.core import setup

setup(
    name='bio-info',
    version='0.0.1',
    description="Biological information management",
    author='Vittorio Zamboni',
    author_email='vittorio.zamboni@gmail.com',
    license='MIT',
    url='',
    packages=[
        'bio_info',
    ],
)

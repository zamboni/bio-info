import os


class InvalidResourceTypeError(Exception):
    """ Raises if the resource type is not a valid type. """
    pass


class InvalidResourceSourcePathError(Exception):
    """ Raises when trying to get resource information from an invalid path. """
    pass


"""
List of supported types:
 - sam: Sequence Alignment/Map (samtools.sourceforge.net)
 - bam: binary sam
 - gff3: General Feature Format ver. 3 (sequenceontology.org/gff3.shtml)
 - dcr: Data Chunk Reference (bitbucket.org/iga/python-dcr)
 - wig: wiggle (genome.ucsc.edu/goldenPath/help/wiggle.html)
 - bigwig: binary wiggle
"""
RESOURCE_TYPES = {
    'sam': {'category': 'alignment',
            'module': 'sam',
            'class': 'SAMInfo',
            'description': 'Sequence Alignment/Map'},
    'bam': {'category': 'alignment',
            'module': 'sam',
            'class': 'BAMInfo',
            'description': 'Binary SAM'},
    'gff3': {'category': 'annotation',
             'module': 'gff3',
             'class': 'GFF3Info',
             'description': 'General Feature Format v3'},
    'dcr': {'category': 'profile',
            'module': 'dcr',
            'class': 'DCRInfo',
            'description': 'Data Chunk Reference'},
    'wig': {'category': 'profile',
            'module': 'wig',
            'class': 'WIGInfo',
            'description': 'WIGGLE'},
    'bigwig': {'category': 'profile',
               'module': 'wig',
               'class': 'BigWIGInfo',
               'description': 'Binary WIGGLE'},
}


def GetResource(type_name, source_path, a_get=True, **kwargs):
    """Creates a Resource instance or a parser sublcassed instance.

    To obtain a GFF3Info instance, use:
    >>> g = GetResource('gff3', '/path/to/annotations.gff3')  # GFF3Info instance
    Or:
    >>> r = GetResource('gff3', '/path/to/annotations.gff3', False)  # Resource instance
    >>> g = r.get_annotation_resource()  # GFF3Instance
    """
    
    r = Resource(type_name, source_path, **kwargs)
    if a_get:
        return r.get_annotation_resource()
    return r


class Resource(object):
    """A wrapper to a biological related resource.

    Resource information:
        - type_name: name of element type
        - kwargs: parser info kwargs
    """

    def __init__(self, type_name, source_path, **kwargs):
        self.type_name = type_name
        self.source_path = source_path
        self.parser_kwargs = kwargs

    @property
    def resource_type(self):
        if self.type_name in RESOURCE_TYPES:
            return RESOURCE_TYPES[self.type_name]
        else:
            raise InvalidResourceTypeError(
                "Type '%s' is not a valid element type" % self.type_name)

    @property
    def resource_type_class(self):
        class_import = __import__(
            'bio_info.parser.%s' % self.resource_type['module'],
            fromlist=[self.resource_type['class']])
        return getattr(class_import, self.resource_type['class'])

    def get_annotation_resource(self):
        if not self.source_path:
            raise InvalidResourceSourcePathError(
                    "Element source path is missing")
        if not os.path.exists(self.source_path):
            raise InvalidResourceSourcePathError(
                    "Invalid source path: '%s'" % self.source_path)
        ResourceTypeClass = self.resource_type_class
        return ResourceTypeClass(self.source_path, **self.parser_kwargs)

    def validate_data(self):
        pass

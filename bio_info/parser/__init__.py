from abc import ABCMeta, abstractmethod


class InvalidParserInfoCoordinatesError(Exception):
    """ Raises if the resource coordinates are not coherent. """
    pass


class ParserInfo(object):
    """Parser element information:
        - source_path: path of the source element
        - strand: '+', '-' or ''
    Sequence information
        - reference_sequence: sequence in range start-end of the reference
        - reference_path: path to the refence file
    Start and end position of the block, in genome coordinates
        - start: included first position
        - end: included last position
        - length
    """

    __metaclass__ = ABCMeta

    def __init__(self, source_path, **kwargs):
        self.source_path = source_path
        self.strand = kwargs.get('strand', '')
        self.reference_sequence = kwargs.get('reference_sequence', '')
        self.reference_path = kwargs.get('reference_path', '')

        self.tags = []

        self.start = 0
        self.end = 0
        self.length = 0
        self.update_coordinates(kwargs.get('start', 0), kwargs.get('end', -1),
            kwargs.get('length', 0))

    def validate_coordinates(self):
        if max(self.start, self.end) - min(self.start, self.end) != self.length + 1 \
                and self.start and self.end:
            raise InvalidParserInfoCoordinatesError(
                    "start (%s) + end (%s) -1 are different from length %s" %
                    (self.start, self.end, self.length))

    def update_coordinates(self, start, end, length):
        if start and end and not length:
            if self.strand == '+':
                self.length = self.end - self.start + 1
            else:
                self.length = self.start - self.end + 1
        elif self.start and not self.end and self.length:
            if self.strand == '+':
                self.end = self.start + self.length - 1
            else:
                self.end = self.start - self.length + 1
        elif not self.start and self.end and self.length:
            if self.strand == '+':
                self.start = self.end - self.length + 1
            else:
                self.start = self.end + self.length - 1
        self.validate_coordinates()

    @abstractmethod
    def load_elements(self):
        pass

    def update_tags(self, tags):
        """ Update element tags. """
        self.tags.update(tags)

from bio_info.parser import ParserInfo
from python_dcr import dcr 

class DCRInfo(ParserInfo):

    EXPORT_FORMATS = []

    def __init__(self, source_path, **kwargs):
        super(DCRInfo, self).__init__(source_path, **kwargs)
        self.annotations = []

    def load_elements(self, **kwargs):
        if not self.source_path:
            raise Exception("A gff path is required.")
        self.annotations = []
        data = {}
        # Get location information: chr01:100..120 from deletions
        data['location'] = kwargs.setdefault('location', None)  # 'chr01'
        data['start'] = int(kwargs.setdefault('start', 1))  # 100
        data['end'] = int(kwargs.setdefault('end', -1))  # 120

        dcr_resource = dcr.DCRFile(self.source_path)
        self.annotations = dcr_resource.fetch(data['location'], data['start'], data['end'])
        del dcr_resource

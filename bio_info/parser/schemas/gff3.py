from ..schemas import Schema


class GFF3GenesSchema(Schema):
    _ELEMENTS = {
        'gene': {
            'order': ['mRNA'],
            'elements': {
                'mRNA': {
                    'order': ['five_prime_UTR', 'exon', 'CDS', 'three_prime_UTR'],
                    'elements': {
                        'five_prime_UTR': {},
                        'exon': {},
                        'CDS': {},
                        'three_prime_UTR': {}, }}}}}


class GFF3MatchPartSchema(Schema):
    _ELEMENTS = {
        '%smatch': {
            'order': ['match_part'],
            'elements': {
                'match_part': {}, }}}

    def __init__(self, match_name=''):
        super(GFF3MatchPartSchema, self).__init__()
        
        for element_name in self.ELEMENTS.keys():
            print element_name, match_name
            if element_name % match_name != element_name:
                self.ELEMENTS[element_name % match_name] = self.ELEMENTS[element_name]
                del self.ELEMENTS[element_name]

GFF3_SCHEMAS = {
    'genes': GFF3GenesSchema,
    'match_part': GFF3MatchPartSchema,
}

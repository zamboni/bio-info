from abc import ABCMeta, abstractmethod, abstractproperty
from copy import deepcopy

class Schema(object):
    """Basic schema information
    """

    __metaclass__ = ABCMeta

    _ELEMENTS = {}

    def __init__(self):
        self.ELEMENTS = deepcopy(self._ELEMENTS)

    def get_structure(self):
        return self.ELEMENTS

    @property
    def parents(self):

        def _get_parents(parent_name, parent_element, parents_dict): 
            if parent_element:
                for element_son_name in parent_element['order']:
                    parents_dict[element_son_name] = parent_name
                    _get_parents(element_son_name,
                                 parent_element['elements'][element_son_name], parents_dict)

        parents_dict = {}
        structure = self.get_structure()
        print structure
        for element_name in structure:
            print element_name
            parents_dict[element_name] = None
            _get_parents(element_name, structure[element_name], parents_dict)
        return parents_dict

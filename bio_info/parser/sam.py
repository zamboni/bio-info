from django.conf import settings
from operator import itemgetter

from .models import GenomeElement


CIGAR_CODE = {
    0: {'code': 'M',
        'desc': "alignment match (can be a sequence match or mismatch)"},
    1: {'code': 'I',
        'desc': "insertion to the reference"},
    2: {'code': '2',
        'desc': "deletion from the reference"},
    3: {'code': 'N',
        'desc': "skipped region from the reference"},
    4: {'code': 'S',
        'desc': "soft clipping (clipped sequences present in SEQ)"},
    5: {'code': 'H',
        'desc': "hard clipping (clipped sequences NOT present in SEQ)"},
    6: {'code': 'P',
        'desc': "padding (silent deletion from padded reference)"},
    7: {'code': '=',
        'desc': "sequence match"},
    8: {'code': 'X',
        'desc': "sequence mismatch"},
}


def genome_element_ratio(max_element_length, max_pixels):
    ratio = float(float(max_pixels) / float(max_element_length))
    return ratio


def genome_set_longest_element(genome, genome_elements=None):
    if not genome_elements:
        genome_elements = GenomeElement.objects.filter(
                genome=genome).order_by('name')
    if genome_elements:
        max_element = genome_elements[0]
        for element in genome_elements:
            if element.length > max_element.length:
                max_element = element
            genome.longest_element = max_element
        genome.save()
    return genome


def genome_element_pixels(genome, max_pixels=None, ratio=None):
    elements = GenomeElement.objects.filter(genome=genome).order_by('name')
    elements_dict = []
    if elements:
        if not max_pixels:
            max_pixels = settings.VBROWSE_IMAGES_WIDTH
        if not ratio:
            if not genome.longest_element:
                genome = genome_set_longest_element(genome, elements)
            ratio = genome_element_ratio(genome.longest_element.length,
                                         max_pixels)
        for element in elements:
            elements_dict.append({'name': element.name,
                                  'length': element.length,
                                  'codename': element.codename,
                                  'pixels': int(element.length * ratio),
                                  })
    return elements_dict


def blocks_from_bam(bam_path, region_name,
                   genome_start_position, genome_end_position, **kwargs):
    from vbrowse.render_models import PrintableBlock, PrintableBlockElement, \
            create_reads_link

    read_sequences = kwargs.get('read_sequences', False)
    keep_element = kwargs.get('keep_element', False)

    import pysam
    samfile = pysam.Samfile(bam_path, "rb")

    # Dictionary of reads
    reads = {}
    # Dictionary of reads, sorted by order of appearence (if sam is sorted,
    # same as genome)
    sorted_reads = {}
    names = []
    order = 0
    num_read = 0
    for samread in samfile.fetch(bytes(region_name),
            genome_start_position, genome_end_position):
        num_read += 1
        #print "parsed %s - %s" % (num_read, samread.qname)

        '''
        Read could be paired (.is_paired) or not. If it is not paired,
        it is considered as a single read. In the same way paired reads
        could have an unmapped mate (.mate_is_unmapped), so are considered
        as single (but marked with a different tag).
        Each read could be in the main strand or in reverse (.is_reverse).
        In order to color in different ways all these elements, a 'type'
        is saved in the block element.
        '''
        read_name = samread.qname
        if not samread.is_paired:
            read_name += '_U'
        names.append(read_name)
        read_start = samread.pos + 1
        # The read end is the min of the real end and the 'paper' end
        r_end = samread.alen
        if not r_end:
            r_end = samread.qlen
        read_end = min(samread.pos + r_end, genome_end_position)
        read_block = PrintableBlockElement(read_start, read_end,
                                           element_type='read')
        read_block.set_name(samread.qname)
        try:
            # A read with this name has already created a block
            block = reads[read_name]
        except:
            block = PrintableBlock(read_start, read_end,
                                   samread.qname, element_type='read')
            reads[read_name] = block
            sorted_reads[order] = read_name
            order += 1
        if samread.is_paired:
            if samread.mate_is_unmapped:
                read_block.update_info(block_tag='mate_unmapped')
            else:
                if samread.is_proper_pair:
                    read_block.update_info(block_tag='mate_mapped')
                else:
                    read_block.update_info(block_tag='mate_improperly_paired')
                if samread.is_reverse:
                    read_end = samread.pos + r_end
                    read_block.update_coord(end=read_end)
                    read_block.update_info(extra={'strand': 'R'})
                if samread.tid == samread.rnext and \
                        (samread.is_proper_pair or samread.isize < 500):
                    link_block = create_reads_link(
                            samread.pnext, read_block,
                            genome_start_position, genome_end_position)
                    if link_block:
                        block.add_block(link_block)
        else:
            read_block.update_info(block_tag='unpaired')
        if keep_element:
            read_block.set_element(samread)
        rend = samread.aend
        if not rend:
            rend = samread.qend
        extra = {'rstart': samread.pos + 1,
                 'rend': rend,
                 'rlen': samread.rlen,
                 'tlen': rend - samread.pos + 1,
                 'rev': samread.is_reverse,
                 'propp': samread.is_proper_pair}
        if samread.cigar:
            extra['cigar'] = samread.cigar
        read_block.update_info(extra=extra)
        if read_sequences:
            read_block.sequence = samread.query
        block.add_block(read_block)
        reads[read_name] = block
    samfile.close()
    '''
    print 'tot reads: ', str(len(names))
    print 'tot unique names: ', str(len(set(names)))
    print 'tot unique reads: ', str(len(set(reads.keys())))
    '''
    return (reads, sorted_reads)


def gff_parser(gff_path, **kwargs):
    location = kwargs.setdefault('location', None)
    source = kwargs.setdefault('source', None)
    start = int(kwargs.setdefault('start', 1))
    end = int(kwargs.setdefault('end', -1))
    search_parent = kwargs.get('search_parent', False)
    trim_coordinates = kwargs.setdefault('trim_coordinates', False)
    limit_types = kwargs.get('limit_types', [])
    print "LIMIT TYPES %s" % limit_types
    annotations = []
    gff_handler = open(gff_path, 'r')
    parents = []
    for line in gff_handler:
        line = line.strip('\r\n')
        if line[:1] != "#" and line != "":
            line = line.split("\t")
            if (line[0] == location or not location) and \
                    (line[1] == source or not source):
                a_start = int(line[3])
                a_end = int(line[4])
                type_in_types = True
                if limit_types:
                    type_in_types = line[2] in limit_types
                # Create a dictionary with tags of the last column,
                # Name=Value will results in {Name: Value}.
                # has_parent is True if the parent has already been processed
                has_parent = False
                tags = {}
                desc_elem = line[8].split(';')
                for d in desc_elem:
                    da = d.split('=')
                    if len(da) > 1:
                        tags[da[0].strip()] = da[1].strip().\
                                strip('"').strip("'")
                if search_parent:
                    if 'Parent' in tags:
                        if tags['Parent'] in parents:
                            has_parent = True
                # Check if the element is inside the coords or is son of
                # an element in the window
                if (
                        (a_start >= start and a_start <= end) or  # starts in
                        (a_end >= start and a_end <= end) or  # ends in
                        (a_start <= start and a_end >= end) or  # cover
                        end < 0 or has_parent):
                    if type_in_types:
                        if 'ID' in tags:
                            parents.append(tags['ID'])
                        if trim_coordinates:
                            if a_start < start:
                                a_start = start
                            if a_end > end and end > 0:
                                a_end = end
                        annotation = {
                            'location': line[0],
                            'source': line[1],
                            'type': line[2],
                            'start': a_start,
                            'end': a_end,
                            'length': a_end - a_start,
                            'orientation': line[6],
                            'desc': line[8],
                            'tags': tags,
                        }
                        annotations.append(annotation)
                else:
                    if annotations:
                        break
    gff_handler.close()
    return annotations


def blocks_from_gff(gff_path, region_name,
                    genome_start_position, genome_end_position, **kwargs):
    from .models import AnnotationStructure
    from .render_models import PrintableBlock, PrintableBlockElement
    trim_coordinates = kwargs.setdefault('trim_coordinates', False)
    search_parent = kwargs.setdefault('search_parent', False)
    annotation_structure = kwargs.get('annotation_structure', None)
    keep_element = kwargs.get('keep_element', False)
    limit_types = kwargs.get('limit_types', [])
    if isinstance(limit_types, basestring):
        limit_types = limit_types.split(',')

    gff_elements = gff_parser(gff_path,
                              location=region_name,
                              start=genome_start_position,
                              end=genome_end_position,
                              trim_coordinates=trim_coordinates,
                              search_parent=search_parent,
                              limit_types=limit_types)
    tree_structure = kwargs.get('tree_structure', {})
    tree_structure_default = {'parent': None,
                              'sons': [],
                              'sons_exclude': [],
                              }
    tree_structure.update(tree_structure_default)
    '''
    Try to guess the preset '''
    if not annotation_structure:
        element_types = set([element['type'] for element in gff_elements[:20]])
        if 'gene' in element_types and 'mRNA' in element_types:
            annotation_structure = \
                    AnnotationStructure.objects.get(codename='gene')
        if 'match' in element_types and 'match_part' in element_types:
            annotation_structure = \
                    AnnotationStructure.objects.get(codename='match')
    '''
    Real check of the existence '''
    if annotation_structure:
        elements = annotation_structure.elements
        tree_structure = {'parent': annotation_structure.parent,
                          'sons': elements['sons'],
                          'sons_exclude': elements['sons_exclude'],
                          'elements': elements['elements']}

    # Dictionary of 'genes'
    annotations = {}
    # Dictionary of genes, sorted by order of appearence (if sam is sorted,
    # same as genome)
    sorted_annot = {}
    sorted_genes = []
    parents = {}
    order = 0
    iteration = 0
    by_id = {}
    for element in gff_elements:
        iteration += 1
        el_id = str(iteration)
        if tree_structure['parent']:
            el_id = element['tags'].setdefault('ID', str(iteration))
        el_parent = element['tags'].setdefault('Parent', None)
        el_name = element['tags'].setdefault('Name', el_id)
        if el_id and el_parent:
            parents[id] = el_parent
        by_id[el_id] = element
        extra = element['tags']
        if element['type'] == tree_structure['parent'] or \
                not tree_structure['parent']:
            if not el_id:
                el_id = order
            block = PrintableBlock(element['start'], element['end'], el_name)
            if keep_element:
                block.element = element
                if element['type'] == tree_structure['parent']:
                    # Monkey patch
                    if element['type'] == 'mRNA':
                        block.parent_element = by_id[el_parent]
                    else:
                        pass
            extra.update({'orientation': element['orientation']})
            block.update_info(extra=extra)
            block.set_type(element['type'])
            annotations[el_id] = block
            sorted_genes.append({'id': el_id, 'start': element['start']})
            order += 1
        elif element['type'] in tree_structure['sons'] or \
                element['type'] not in tree_structure['sons_exclude']:
            try:
                block = annotations[el_parent]
                elem_block = PrintableBlockElement(element['start'],
                                                   element['end'],
                                                   block_tag=element['type'])
                elem_block.update_info(extra=extra)
                elem_block.set_type(element['type'])
                if keep_element:
                    elem_block.element = element
                block.add_block(elem_block)
            except:
                raise Exception("Error parsing gene")
    sorted_genes = sorted(sorted_genes, key=itemgetter('start'))
    i = 0
    for gene in sorted_genes:
        sorted_annot[i] = gene['id']
        i += 1
    '''
    print 'tot reads: ', str(len(names))
    print 'tot unique names: ', str(len(set(names)))
    print 'tot unique reads: ', str(len(set(reads.keys())))
    '''
    return (annotations, sorted_annot)


def fasta_parser(fasta_path, region_name, start_position, end_position):
    from pyfasta import Fasta
    fasta = Fasta(fasta_path)
    return fasta[region_name][start_position - 1:end_position]


def blocks_from_fasta(fasta_path, region_name,
                      start_position, end_position, **kwargs):
    from vbrowse.render_models import PrintableBlock

    sequences = {}
    sorted_sequences = {}

    sequence = fasta_parser(fasta_path, region_name,
                            start_position, end_position)
    block = PrintableBlock(start_position, end_position,
                           'Genome sequence')
    block.sequence = sequence
    sequences['Genome sequence'] = block
    sorted_sequences[0] = 'Genome sequence'
    '''
    size = genome_end_position - genome_start_position + 1
    start = genome_end_position - 10
    end = start + size - 1 - 10
    sequence = fasta[region_name][start:end]
    block = PrintableBlock(genome_start_position + 5,
                           genome_start_position + 5 + len(sequence),
                           'Test')
    block.update_info(extra={'sequence': sequence})
    sequences['Test sequence'] = block
    sorted_sequences[1] = 'Test sequence'
    '''

    return (sequences, sorted_sequences)


def values_from_dcr(dcr_path, reference_name, start_pos, end_pos):
    from python_dcr import dcr
    f = dcr.DCRFile(dcr_path)
    return f.fetch(reference_name, start_pos, end_pos)


def stats_from_dcr(dcr_path, reference_name):
    from python_dcr import dcr
    f = dcr.DCRFile(dcr_path)
    return f.get_reference_info(reference_name)


def annotation_views_valid_range(annotation_views_ranges):
    valid_range = True
    for i1 in range(0, len(annotation_views_ranges) - 1):
        av1 = annotation_views_ranges[i1]
        for i2 in range(i1 + 1, len(annotation_views_ranges)):
            av2 = annotation_views_ranges[i2]
            #print '%s vs %s' % (av1, av2)
            if  (min(av1[1], av2[1]) >=
                 max(av1[0] + 1, av2[0] + 1)):
                valid_range = False
                '''
                print av1
                print ''
                print av2
                print ''
                '''
                break
    return valid_range

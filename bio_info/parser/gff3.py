from bio_info.parser import ParserInfo

from .schemas.gff3 import GFF3_SCHEMAS


def parse_gff3_plain(gff_handler, data, **kwargs):
    """Parse a GFF3 in plain text (no compression, no index).
    Args:
      - gff_handler: an open handler to a gff3 file
      - data: dictionary with information to extract (see GFF3Info class)
    Kwargs:
      - gff_instance: a GFF3Info class used to load elements in `annotations` attribute 
    """
    gff_instance = kwargs.get('gff_instance', None)
    annotations = []

    start = data['start']
    end = data['end']
    for line in gff_handler:
        line = line.strip('\r\n')
        if line[:1] != "#" and line != "":
            line = line.split("\t")
            if (line[0] == data['location'] or not data['location']) and \
                    (line[1] == data['source'] or not data['source']):
                a_start = int(line[3])
                a_end = int(line[4])
                type_in_types = line[2] in data['limit_types'] or not data['limit_types']
                # Create a dictionary with tags of the last column,
                # Name=Value will results in {Name: Value}.
                # has_parent is True if the parent has already
                # been processed (by ID)
                has_parent = False
                tags = {}
                desc_elem = line[8].split(';')
                for d in desc_elem:
                    da = d.split('=')
                    if len(da) > 1:
                        tags[da[0].strip()] = da[1].strip().strip('"\'')
                if data['search_parent']:
                    if 'Parent' in tags:
                        if tags['Parent'] in data['parents']:
                            has_parent = True
                # Check if the element is inside the coords or is son of
                # an element in the window
                if (
                        (a_start >= start and a_start <= end) or  # starts
                        (a_end >= start and a_end <= end) or  # ends in
                        (a_start <= start and a_end >= end) or  # cover
                        end < 0 or has_parent):
                    if type_in_types:
                        if 'ID' in tags:
                            # Add ID to processed elements so they can be
                            # used as parent
                            data['parents'].append(tags['ID'])
                        if data['trim_coordinates']:
                            if a_start < start:
                                a_start = start
                            if a_end > end and end > 0:
                                a_end = end
                        annotation = {
                            'location': line[0],
                            'source': line[1],
                            'type': line[2],
                            'start': a_start,
                            'end': a_end,
                            'length': a_end - a_start,
                            'orientation': line[6],
                            'desc': line[8],
                            'tags': tags,
                        }
                        if gff_instance:
                            gff_instance.annotations.append(annotation)
                        else:
                            annotations.append(annotations)
                else:
                    # Annotation is not in selected region
                    if annotations:
                        break
                    elif gff_instance:
                        if gff_instance.annotations:
                            # Exit, annotations have been loaded
                            break
        if not gff_instance:
            return annotations
    

class GFF3Info(ParserInfo):

    EXPORT_FORMATS = []

    def __init__(self, source_path, **kwargs):
        super(GFF3Info, self).__init__(source_path, **kwargs)
        self.annotations = []

    def load_elements(self, **kwargs):
        if not self.source_path:
            raise Exception("A gff path is required.")
        self.annotations = []
        data = {}
        # Get location information: chr01:100..120 from deletions
        data['location'] = kwargs.setdefault('location', None)  # 'chr01'
        data['source'] = kwargs.setdefault('source', None)  # 'deletions'
        data['start'] = int(kwargs.setdefault('start', 1))  # 100
        data['end'] = int(kwargs.setdefault('end', -1))  # 120
        # Remove bases outside region
        data['trim_coordinates'] = kwargs.setdefault('trim_coordinates', False)
        # Limit only to a list of types (col 3)
        data['limit_types'] = kwargs.get('limit_types', [])
        # Include elements by parent's name
        data['parents'] = kwargs.get('parents', [])
        data['search_parent'] = kwargs.get('search_parent', False)

        with open(self.source_path, 'r') as gff_handler:
            parse_gff3_plain(gff_handler, data, gff_instance=self)
